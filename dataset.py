
import soundfile as sf
import librosa
import librosa.display

import numpy as np
import matplotlib.pyplot as plt
import torch
import torchvision
from PIL import Image
import os
import sys
from glob import glob
import constants
from tqdm import tqdm
import preprocessing
from torch.utils.data import Dataset
import csv

class MapsDataset(Dataset):
    def __init__(self,path):


        self.path = path

        self.data = []

        print("Loading Maps...("+path+")")

        audio_files = []

        files = os.listdir(path)

        for file in files:
                (filename, ext) = os.path.splitext(file)
                if ext == ".wav":
                    audio_files.append(filename)
               
        for audio_file in tqdm(audio_files):
            self.load(audio_file)

        print("Maps loaded (" + str(len(audio_files)) +") files")


    def load(self, filename):


        audio = self.path + filename+ ".wav"
        txt = self.path + filename+".txt"

        audio,sr = librosa.load(audio, mono=True) # instead of sf.read() actually, librosa force sample_rate , a good thing.
        assert (sr == constants.SAMPLE_RATE)
        spec = librosa.feature.melspectrogram(audio,sr, n_fft=constants.FFT_SIZE, hop_length=constants.MEL_HOP_LENGTH, n_mels=constants.N_MEL)
        #spec = librosa.amplitude_to_db(spec, ref=np.max)


        

        '''
        spec = np.abs(librosa.cqt(audio, sr=sr))
        spec = librosa.amplitude_to_db(spec, ref=np.max)
        librosa.display.specshow(librosa.amplitude_to_db(C, ref=np.max),sr=sr, x_axis='time', y_axis='cqt_note')
        plt.colorbar(format='%+2.0f dB')
        plt.title('Constant-Q power spectrum')
        plt.tight_layout()
        plt.show()
        '''



        '''
        librosa.display.specshow(spec, sr=sr, hop_length=constants.MEL_HOP_LENGTH, x_axis='time', y_axis='mel');
        plt.colorbar(format='%+2.0f dB');
        plt.title(filename)
        plt.show()
        '''

        csv = []
        txt = np.loadtxt(txt,delimiter='\t',skiprows=1)


        if len(txt.shape) == 1:
            onset,offset,note = txt
            csv.append(txt)
        else:
            csv = txt

        self.process(csv,spec)
        

    def process(self,csv,spec):
        
        for onset,offset,note in csv:

            sliced_spec = preprocessing.crop_spectrogram(spec,onset)
            sliced_spec = np.expand_dims(sliced_spec, 0) # <---------------- WE EXPEND DIM ??? CHANNEL
            sliced_spec = torch.from_numpy(sliced_spec)

            notes = [0] * 88
            for onset2,offset2,note2 in csv:
                if onset2 >= onset and onset2 <= onset + constants.BLOCK_SIZE:
                    note_indice = int(note2) - constants.MIN_MIDI
                    notes[note_indice] = 1
            
            notes = torch.FloatTensor(notes) 

            notes =  notes
            sliced_spec = sliced_spec

            self.data.append((sliced_spec,notes)) # int(note) - constants.MIN_MIDI

        
     
    def __getitem__(self, index):
        return self.data[index]


    def __len__(self):
        return len(self.data)




