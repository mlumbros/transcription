import torch
import torchvision
from torch import nn
import torch.nn.functional as F
import constants


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
      
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear((constants.N_MEL // 4) * (constants.BLOCK_SIZE_BIN // 4) * 64, 1000) 
        self.fc2 = nn.Linear(1000, 88)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        out = self.fc2(out)
        #out = self.normalize(out) We normalize only if needed.
        return out
    
    def normalize(self,out):
        out -= out.min(1, keepdim=True)[0]
        out /= out.max(1, keepdim=True)[0]
        return out

