import librosa.display
import matplotlib.pyplot as plt
import numpy
import sys
import constants
import torch

def crop_spectrogram(spectrogram,onset):

    index = (constants.SAMPLE_RATE * onset) / constants.MEL_HOP_LENGTH
    index = int(index)

    result = spectrogram[0:constants.N_MEL,index:int(index+constants.BLOCK_SIZE_BIN)]

    '''
    librosa.display.specshow(result, sr=constants.SAMPLE_RATE, hop_length=512, x_axis='time', y_axis='mel');
    plt.colorbar(format='%+2.0f dB')
    plt.show()
    '''
  
    return result

def detect_onsets(audio):
    onset_frames = librosa.onset.onset_detect(y=audio, sr=constants.SAMPLE_RATE)
    onsets = librosa.frames_to_time(onset_frames, sr=constants.SAMPLE_RATE)
    return onsets


'''
def detect_onsets(spectrogram):
    spectrogram = spectrogram.reshape(spectrogram.shape[1],spectrogram.shape[0])

    onsets = []

    for i in range(0,len(spectrogram)):
        o_frequency_bins = spectrogram[i-1]
        frequency_bins = spectrogram[i]
        time = (i * constants.MEL_HOP_LENGTH) / constants.SAMPLE_RATE

        count = 0
        for y in range(0,len(frequency_bins)):
            diff = frequency_bins[y] - o_frequency_bins[y]
            if (diff >= 2):
                count = count + 1
        
        if (count >= 50):
            onsets.append(time)

    print(len(onsets))
    input("wow")
    return onsets
'''


   



