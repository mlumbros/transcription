import librosa
import librosa.display

import torch
import torchvision
from torch import nn
from torchvision import transforms, datasets
import numpy
import torch.optim as optim
import torch.nn.functional as F
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as image
import sys
from model import Net
from dataset import MapsDataset
import constants
import wave 
import transcribe
import preprocessing
from midiutil.MidiFile import MIDIFile


def view_signal(audio_path):
    spf = wave.open(audio_path, "r")
    signal = spf.readframes(-1)
    signal = numpy.fromstring(signal, "Int16")
    plt.figure(1)
    plt.title("Signal Wave...")
    plt.plot(signal)
    plt.show()

def eval_model():

    net = Net()
    net.load_state_dict(torch.load("model.pt"))
    print("Network loaded.")

    net.eval()

    test = MapsDataset("MAPS_TEST/")
    
    testset = torch.utils.data.DataLoader(test, batch_size=10, shuffle=False)

    acc = compute_accuracy(net,testset)

    print("Accuracy: " + str(acc))
    
def train():
    
    train = MapsDataset("MAPS/")
    test = MapsDataset("MAPS_TEST/")
    
    trainset = torch.utils.data.DataLoader(train, batch_size=40, shuffle=True)
    testset = torch.utils.data.DataLoader(test, batch_size=40, shuffle=False)

    print("Starting training...")

    net = Net()

    # Too few epochs, and your model wont learn everything it could have.
    # Too many epochs and your model will over fit to your in-sample data 
    EPOCHS = 30
    # learning rate dictate the magnitude of changes optimizer can make at a time,
    # to high learning rate can lead to chaotic gradiant descent
    LEARNING_RATE = 0.001

    #loss_function = nn.BCELoss()
    loss_function = nn.MSELoss() # euclidian distance,allow multi label.
    #loss_function = nn.CrossEntropyLoss()

    optimizer = optim.Adam(net.parameters(), lr=LEARNING_RATE) 

    for epoch in range(EPOCHS): # We iterate 3 time over all our training data data
        for i,data in enumerate(trainset):  # iterate over training data

            # X is cnn input data (a window of spectrogram)
            # y is expected result (a note)
            X, y = data  

            net.zero_grad()  # sets gradients to 0 before loss calc every step
            output = net(X)  # pass in the reshaped batch (Width = 100 (window size) Height = 229 (log scale))
            
            loss = loss_function(output,y)  # calc and grab the loss value
            loss.backward()  # apply this loss backwards thru the network's parameters
            optimizer.step()  # attempt to optimize weights 
        
        print("Epoch: "+ str(epoch+1)+ " "+str(loss))

    torch.save(net.state_dict(), "model.pt")

    accuracy = compute_accuracy(net,testset)
    print("Accuracy: " + str(accuracy))

def compute_accuracy(net,testset):
    correct = 0
    total = 0

    with torch.no_grad():

        total = 0
        correct = 0

        threshold = 0.9

        for data in testset:
            X, y = data # X is i cropped spectrogram. y is the associated midi notes

            output = net(X)

            for i in range(0,88):
                if y[i] == 1:
                    total += 1

                    if output[i] >= threshold:
                        correct += 1
                        
    return round(correct/total, 3)

def process_file(file_name):
        
    net = Net() 
    net.load_state_dict(torch.load("model.pt"))
    net.eval()

    audio,sr = librosa.load(file_name, mono=True)
    assert(sr == constants.SAMPLE_RATE)

    spec = librosa.feature.melspectrogram(audio,sr, n_fft=constants.FFT_SIZE, hop_length=constants.MEL_HOP_LENGTH, n_mels=constants.N_MEL)

    notes = []
    with torch.no_grad():
        onsets = preprocessing.detect_onsets(audio)
        for onset in onsets:
            sliced_spec = preprocessing.crop_spectrogram(spec,onset)
            sliced_spec = numpy.expand_dims(sliced_spec, 0)
            sliced_spec = numpy.expand_dims(sliced_spec, 0) 
            sliced_spec = torch.from_numpy(sliced_spec)

            result = net(sliced_spec)
            result -= result.min(1, keepdim=True)[0]
            result /= result.max(1, keepdim=True)[0]
            
            for i,note in enumerate(result[0]):
                 if (note >= 0.9): # this value should be a constant (its a threshold)
                    note = i + constants.MIN_MIDI
                    notes.append((onset,note))

        mf = MIDIFile(1)   
         
        track = 0   

        time = 0    
        mf.addTrackName(track, time, "Sample Track")
        mf.addTempo(track, time, 50)

        for onset,note in notes:

            channel = 0
            volume = 100
            pitch = note          
            time = onset          
            duration = 0.5       
            mf.addNote(track, channel, pitch, time, duration, volume)

        with open("output.mid", 'wb') as outf:
                mf.writeFile(outf)   
                
eval_model()
 

